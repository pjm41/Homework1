package nLWest;

public abstract class Team{
	protected String placement;
	private String teamName;
	private double winningPercentage;
	private int totalWins;
	
	/*public Team(String place) {
		this.placement = place;
	}*/
	
	public Team(double percent){
		this.winningPercentage = percent;
	}
	
	public Team(double percent, String name){
		this.winningPercentage = percent;
		this.teamName = name;
	}
	
	public String getPlacement(){
		return this.placement;
	}
	
	public double getWinningPercentage(){
		return this.winningPercentage;
	}
	
	public void setTotalWins(int wins){
		this.totalWins = wins;
	}
	
	public void setPlacement(String place){
		this.placement = place;
	}
	
	public String getName(){
		return this.teamName;
	}
	
	public abstract void printPlacement();

	public abstract void printName();
	
}
