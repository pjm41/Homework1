import nLWest.DBacks;
import nLWest.Dodgers;
import nLWest.Giants;
import nLWest.Padres;
import nLWest.Rockies;
import nLWest.Team;
//import java.lang.Object;
import java.util.Random;


public class main {
	public static void main(final String[] args) {
		
		double rangeMin = 0;
		double rangeMax = 1;
		
		double rangeMinError = -1;
		double rangeMaxError = 1;
		
		Team[] teams = new Team[5];
		Team giants = new Giants(0.519, "Giants");
		Team dodgers = new Dodgers(0.568, "Dodgers");
		Team dbacks = new DBacks(0.488, "DBacks");
		Team rockies = new Rockies(0.457, "Rockies");
		Team padres = new Padres(0.420, "Padres");
		
		teams[0] = giants;
		teams[1] = dodgers;
		teams[2] = dbacks;
		teams[3] = rockies;
		teams[4] = padres;
		
		for(Team team : teams ){
			int tmpWins = 0;
			for (int i=0; i<162; i++) {
				Random r = new Random();
				double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
				if (randomValue <= team.getWinningPercentage()){
					tmpWins++;
				}
			}
			Random r = new Random();
			double randomValueError = rangeMinError + (rangeMaxError - rangeMinError) * r.nextDouble();
			randomValueError *= 10;
			tmpWins += randomValueError;
			System.out.println("The " + team.getName() + " have " + tmpWins + " wins");
			//System.out.println("The " + team.getName() + " have " + team.get + " wins");
			team.setTotalWins(tmpWins);
		}
			
	}
}
